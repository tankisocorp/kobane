
let input = document.querySelector('#input');

let searchBtn = document.querySelector('#search');

let apiKey = 'bb881b95-c92b-413a-8d2d-20d7cfe5c963';

let notFound = document.querySelector('.not__found');
let defBox = document.querySelector('.def');

let audioBox = document.querySelector('.audio');

let loading = document.querySelector('.loading');


searchBtn.addEventListener('click', function(e){
    e.preventDefault();
    
    // clear data

    audioBox.innerHTML = '';
    notFound.innerText = '';
    defBox.innerText= '';

    // Get input data

    let word = input.value;

    // call API get data

    if (word == ''){
        alert("Please enter an English word");
        return;
    }
    
    getData(word);
})

async function getData(word){
    loading.style.display = 'block';
    // Ajax call

    const response = await fetch ('https://dictionaryapi.com/api/v3/references/sd4/json/'+word+'?key='+apiKey);
    const data = await response.json();
    console.log(data);

    // if empty result

    if(!data.length){
        loading.style.display = 'none';
        notFound.innerText = 'No result found';
        return;
    }

    if (typeof data[0] === 'string'){
        loading.style.display = 'none';
        let heading = document.createElement('h3');
        heading.innerText = "Did you mean?";
        notFound.appendChild(heading);
        data.forEach(element => {
            let suggestion = document.createElement('span');
            suggestion.classList.add('suggested');
            suggestion.innerText = element;
            notFound.appendChild(suggestion);
        });
    }

    // Result found
    loading.style.display = 'none';
    let definition = data[0].shortdef[0];
    defBox.innerText = definition;

    // sound
    const soundName = data[0].hwi.prs[0].sound.audio;
    if(soundName){
        renderSound(soundName);
    }
    console.log(data);
}

function renderSound(soundName){
    let subfolder = soundName[0];
    //let soundSrc = 'https://media.merriam-webster.com/soundc11/'+subfolder+'/'+soundName+'.wav?key='+apiKey;
    let soundSrc = 'https://media.merriam-webster.com/audio/prons/en/us/wav/'+subfolder+'/'+soundName+'.wav';
    //https://media.merriam-webster.com/audio/prons/[language_code]/[country_code]/[format]/[subdirectory]/[base filename].[format]

    let aud = document.createElement('audio');
    aud.src= soundSrc;
    aud.controls = true;
    audioBox.appendChild(aud);
}
